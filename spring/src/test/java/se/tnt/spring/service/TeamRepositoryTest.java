package se.tnt.spring.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.tnt.spring.TestConfig;
import se.tnt.spring.exception.ServiceException;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;
import se.tnt.spring.status.Status;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class TeamRepositoryTest {


    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private MasterService service;

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WorkItemRepository workItemRepository;

    private User correctUser;
    private Team dreamteam;
    private WorkItem workItem;

    @Before
    public void setup() {
        dreamteam = new Team("dreamteam");
        workItem = new WorkItem("dumpster trash secret code");
        correctUser = new User("Joanneisasillygoose", "Anna", "supersecretlongusername", "supersecretshortpassword");
        service.saveTeam(dreamteam);

//        service.saveUser(correctUser);
        dreamteam.addUser(correctUser);

        service.saveTeam(dreamteam);
        service.saveWorkItem(workItem);
    }

    @After
    public void tearDown() {
        teamRepository.findAll().forEach(team -> teamRepository.delete(team));
        userRepository.findAll().forEach(user -> userRepository.delete(user));
        workItemRepository.findAll().forEach(workItem -> workItemRepository.delete(workItem));
    }

    @Test
    public void teamShouldNotHaveMoreThan10Users() {
        for (int i = 0; i < 10; i++) {
            User u = new User("haljdhfblajksdhfkjs" + i, "annapanna", "lindqvist", "password");
            dreamteam.addUser(u);
        }
        exception.expect(ServiceException.class);
        service.saveTeam(dreamteam);
    }

    @Test
    public void userCanChangeTeam() {
        assertThat(service.findUsersByTeam(dreamteam), contains(correctUser));
        Team secondTeam = new Team("SecondTeam");
        correctUser = service.findByUserNumber(correctUser.getUserNumber());
        service.saveTeam(secondTeam);
        secondTeam = service.findByTeamName(secondTeam.getTeamName());
        secondTeam.addUser(correctUser);
        service.saveTeam(secondTeam);
        assertThat(service.findUsersByTeam(secondTeam), contains(correctUser));

    }

    @Test
    public void canInactivateTeam() {
        dreamteam = service.findByTeamName("dreamteam");
        dreamteam.setStatusInactive();
        service.saveTeam(dreamteam);
        assertTrue(service.findTeamByStatus(Status.Inactive).contains(dreamteam));
    }

}