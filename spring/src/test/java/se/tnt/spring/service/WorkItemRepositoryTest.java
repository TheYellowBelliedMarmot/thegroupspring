package se.tnt.spring.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.tnt.spring.TestConfig;
import se.tnt.spring.exception.ServiceException;
import se.tnt.spring.model.Issue;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;
import se.tnt.spring.status.Status;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class WorkItemRepositoryTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private MasterService service;

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WorkItemRepository workItemRepository;

    private User incorrectUser;
    private User correctUser;
    private Team t;
    private WorkItem workItem;

    @Before
    public void setup() {
        incorrectUser = new User("frodr", "kkk", "ys", "xxxxx");
        t = new Team("dreamteam");
        workItem = new WorkItem("dumpster trash secret code");
        correctUser = new User("Joanneisasillygoose", "Anna", "supersecretlongusername", "supersecretshortpassword");
        service.saveUser(correctUser);
        service.saveTeam(t);
        service.saveWorkItem(workItem);
    }

    @After
    public void tearDown() {
        teamRepository.findAll().forEach(team -> teamRepository.delete(team));
        userRepository.findAll().forEach(user -> userRepository.delete(user));
        workItemRepository.findAll().forEach(workItem -> workItemRepository.delete(workItem));
    }

    @Test
    public void canChangeStatusOnWorkItem() {
        assertFalse(service.findWorkItemsByStatus(Status.Done).contains(workItem));
        workItem.setStatusDone();
        service.saveWorkItem(workItem);
        assertTrue(service.findWorkItemsByStatus(Status.Done).contains(workItem));
    }

    @Test
    public void workItemCanBeRemoved() {
        WorkItem workItem = new WorkItem("do this thing");
        assertThat(service.saveWorkItem(workItem), not(equalTo(null)));
        workItem = service.findWorkItemById(workItem.getId());

        service.deleteWorkItem(workItem);

        assertThat(service.findWorkItemById(workItem.getId()), equalTo(null));
    }

    @Test
    public void getWorkItemsByStatus() {
        assertThat(service.findWorkItemsByStatus(Status.Unstarted), contains(workItem));
    }

    @Test
    public void findAllWorkItemsByTeam() {
        t.addUser(correctUser);
        correctUser.addWorkItem(workItem);
        service.saveTeam(t);
        assertThat(service.findWorkItemsByTeam(t), contains(workItem));
    }

    @Test
    public void findWorkItemsByUser() {
        t.addUser(correctUser);
        correctUser.addWorkItem(workItem);
        service.saveTeam(t);
        assertThat(service.findWorkItemsByUser(correctUser), contains(workItem));
    }

    @Test
    public void workItemsAreSearchable() {
        assertThat(service.searchForWorkItemByDescription("trash"), not(equalTo(null)));
    }

    @Test
    public void shouldNotBeAbleToAddWorkItemsToInactiveUserAndSaveToService() {
        correctUser.setStatusInactive();
        service.saveUser(correctUser);
        correctUser.addWorkItem(workItem);

        exception.expect(ServiceException.class);

        service.saveUser(correctUser);
    }

    @Test
    public void userShouldNotHaveMoreThan5WorkItems() {
        IntStream.range(0, 6).mapToObj(i -> new WorkItem("do it " + i)).forEach(workItem1 -> correctUser.addWorkItem(workItem1));
        exception.expect(ServiceException.class);
        service.saveUser(correctUser);
    }

    @Test
    public void issueShouldBeAddable() {
        workItem.addIssue(new Issue("ERROR: hej too simple"));
        workItem.setStatusDone();
        service.saveWorkItem(workItem);
        assertThat(service.findWorkItemById(workItem.getId()).getIssue(), not(equalTo(null)));
    }

    @Test
    public void findAllWorkItemsWithIssue() {
        assertThat(service.findAllWorkItemsWithIssue(), not(contains(workItem)));
        workItem.addIssue(new Issue("nextlevelissue"));
        workItem.setStatusDone();
        service.saveWorkItem(workItem);
        assertThat(service.findAllWorkItemsWithIssue(), contains(workItem));
    }

    @Test
    public void canOnlyAddAnIssueToWorkItemWithStatusDone() {
        workItem.addIssue(new Issue("is this real life?"));
        exception.expect(ServiceException.class);
        service.saveWorkItem(workItem);
    }

    @Test
    public void workItemStatusShouldBeUnstartedWhenIssueIsAdded() {
        assertThat(workItem.getWorkItemStatus(), equalTo(Status.Unstarted));
        workItem.setStatusDone();
        service.saveWorkItem(workItem);
        assertThat(service.findWorkItemById(workItem.getId()).getWorkItemStatus(), equalTo(Status.Done));
        workItem = service.findWorkItemById(workItem.getId());
        workItem.addIssue(new Issue("is this fake life?"));
        service.saveWorkItem(workItem);
        assertThat(service.findWorkItemById(workItem.getId()).getWorkItemStatus(), equalTo(Status.Unstarted));
    }


}
