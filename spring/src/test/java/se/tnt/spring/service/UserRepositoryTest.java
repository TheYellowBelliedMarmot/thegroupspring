package se.tnt.spring.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.tnt.spring.TestConfig;
import se.tnt.spring.exception.ServiceException;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;
import se.tnt.spring.status.Status;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class UserRepositoryTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    private MasterService service;

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WorkItemRepository workItemRepository;

    private User incorrectUser;
    private User correctUser;
    private Team t;
    private WorkItem workItem;

    @Before
    public void setup() {
        incorrectUser = new User("frodr", "kkk", "ys", "xxxxx");
        t = new Team("dreamteam");
        workItem = new WorkItem("dumpster trash secret code");
        correctUser = new User("Joanneisasillygoose", "Anna", "supersecretlongusername", "supersecretshortpassword");
        service.saveUser(correctUser);
        service.saveTeam(t);
        service.saveWorkItem(workItem);
    }

    @After
    public void tearDown() {
        teamRepository.findAll().forEach(team -> teamRepository.delete(team));
        userRepository.findAll().forEach(user -> userRepository.delete(user));
        workItemRepository.findAll().forEach(workItem -> workItemRepository.delete(workItem));
    }

    @Test
    public void usernameShouldNotExceed10Characters() {
        exception.expect(ServiceException.class);
        service.saveUser(incorrectUser);
    }

    @Test
    public void userShouldNotHaveMoreThan5WorkItems() {

        assertTrue(service.findUserByUsername(correctUser.getUsername()) != null);
        User retrieved = service.findUserByUsername(correctUser.getUsername());

        for (int i = 0; i < 6; i++) {
            retrieved.addWorkItem(new WorkItem(Integer.toString(i)));
        }

        exception.expect(ServiceException.class);
        service.saveUser(incorrectUser);
    }

    @Test
    public void userShouldBeRetrievableByNumber() {
        assertThat(service.findByUserNumber(correctUser.getUserNumber()), not(equalTo(null)));
    }

    @Test
    public void searchOnUserVariables() {
        assertThat(service.searchForUser(correctUser.getFirstName()), not(equalTo(null)));
    }

    @Test
    public void userShouldBeRetrievalbeByTeam() {
        t.addUser(correctUser);
        service.saveUser(correctUser);

        assertThat(service.findUsersByTeam(t), contains(correctUser));
    }

    @Test
    public void workItemsOfInactiveUserShouldBeUnstarted() {
        workItem.setStatusStarted();
        correctUser.addWorkItem(workItem);
        service.saveUser(correctUser);
        assertTrue(service.findWorkItemsByStatus(Status.Started).contains(workItem));

        service.inactivateUser(correctUser);
        service.saveUser(correctUser);
        assertTrue(service.findWorkItemsByStatus(Status.Unstarted).contains(workItem));

    }

}