package se.tnt.spring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import se.tnt.spring.model.IssueTest;
import se.tnt.spring.model.TeamTest;
import se.tnt.spring.model.UserTest;
import se.tnt.spring.model.WorkItemTest;
import se.tnt.spring.service.TeamRepositoryTest;
import se.tnt.spring.service.UserRepositoryTest;
import se.tnt.spring.service.WorkItemRepositoryTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TeamRepositoryTest.class,
        UserRepositoryTest.class,
        WorkItemRepositoryTest.class,
        IssueTest.class,
        TeamTest.class,
        UserTest.class,
        WorkItemTest.class
})
public class TestSuite {
}
