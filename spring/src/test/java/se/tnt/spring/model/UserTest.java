package se.tnt.spring.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User user1;
    private User user2;

    @Before
    public void setup() {
        user1 = new User("a", "b", "c", "d");
        user2 = new User("a", "v", "r", "w");
    }

    @Test
    public void testEquals() throws Exception {
        assertEquals(user1, user2);
    }

    @Test
    public void testHashCode() throws Exception {
        assertEquals(user1.hashCode(), user2.hashCode());
    }

}