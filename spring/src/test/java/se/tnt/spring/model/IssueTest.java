package se.tnt.spring.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;


public class IssueTest {

    private Issue issueOne;
    private Issue issueTwo;

    @Before
    public void setup() {
        issueOne = new Issue("one");
        issueTwo = new Issue("one");
    }

    @Test
    public void testEquals() throws Exception {
        assertThat(issueOne, equalTo(issueTwo));
    }

    @Test
    public void testHashCode() throws Exception {
        assertThat(issueOne.hashCode(), equalTo(issueTwo.hashCode()));
    }

}