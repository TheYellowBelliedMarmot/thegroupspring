package se.tnt.spring.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WorkItemTest {

    private WorkItem workItem1;
    private WorkItem workItem2;

    @Before
    public void setup() {
        workItem1 = new WorkItem("1");
        workItem2 = new WorkItem("1");
    }

    @Test
    public void testEquals() throws Exception {
        assertEquals(workItem1, workItem2);
    }

    @Test
    public void testHashCode() throws Exception {
        assertEquals(workItem1.hashCode(), workItem2.hashCode());
    }

}