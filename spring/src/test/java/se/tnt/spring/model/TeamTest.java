package se.tnt.spring.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeamTest {

    private Team team1;
    private Team team2;

    @Before
    public void setup() {
        team1 = new Team("one");
        team2 = new Team("one");
    }

    @Test
    public void testEquals() throws Exception {
        assertEquals(team1, team2);
    }

    @Test
    public void testHashCode() throws Exception {
        assertEquals(team1.hashCode(), team2.hashCode());
    }

}