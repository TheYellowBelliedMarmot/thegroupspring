package se.tnt.spring.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import se.tnt.spring.model.Team;

import java.util.Collection;

public interface TeamRepository extends JpaRepository<Team, Long> {

    @Query("select t from Team t left join fetch t.users where t.teamName = ?1")
    Team findByTeamName(String teamName);

    Collection<Team> findByTeamStatus(String teamStatus);
}
