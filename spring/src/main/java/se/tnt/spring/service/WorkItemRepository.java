package se.tnt.spring.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;

import java.util.Collection;

public interface WorkItemRepository extends JpaRepository<WorkItem, Long> {

    Collection<WorkItem> findByWorkItemStatus(String status);

    @Query("select w from WorkItem w join w.user u where u.team = ?1")
    Collection<WorkItem> findByTeam(Team t);

    Collection<WorkItem> findByUser(User u);

    Collection<WorkItem> findByDescriptionContaining(String desc);

    Collection<WorkItem> findByIssueNotNull();

}
