package se.tnt.spring.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.tnt.spring.exception.ServiceException;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;
import se.tnt.spring.status.Status;
import se.tnt.spring.validator.Validator;

import java.util.Collection;

@Service
public class MasterService {

    private UserRepository userRepository;
    private WorkItemRepository workItemRepository;
    private TeamRepository teamRepository;

    @Autowired
    public MasterService(UserRepository userRepository, WorkItemRepository workItemRepository, TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.workItemRepository = workItemRepository;
        this.teamRepository = teamRepository;
    }

    /* USER METHODS */
    public User saveUser(User u) {

        if (Validator.valid(u)) {
            return userRepository.save(u);
        } else {
            throw new ServiceException("Invalid user");
        }
    }

    public User findUserByUsername(String u) {
        return userRepository.findByUsername(u);
    }

    public User findByUserNumber(String un) {
        return userRepository.findByUserNumber(un);
    }

    public Collection<User> findUsersByTeam(Team t) {
        return userRepository.findByTeam(t);
    }

    public User inactivateUser(User u) {
        User retrieved;
        if ((retrieved = userRepository.findByUserNumber(u.getUserNumber())) != null) {
            if (retrieved.getWorkItems().size() > 0) {
                removeAllWorkItemsFromUser(u);
            }
        }

        return retrieved;
    }

    private void removeAllWorkItemsFromUser(User u) {
        for (WorkItem workItem : u.getWorkItems()) {
            workItem.setStatusUnstarted();
            u.deleteWorkItem(workItem);
            workItemRepository.save(workItem);
        }
    }
    /* END OF USER METHODS */

    /* TEAM METHODS */
    public Team saveTeam(Team t) {
        if (Validator.valid(t)) {
            return teamRepository.save(t);
        } else {
            throw new ServiceException("Invalid team");
        }
    }

    public Collection<Team> findAllTeams() {
        return teamRepository.findAll(); /* kolla om kan göra pagable */
    }

    public Collection<Team> findTeamByStatus(String status) {
        return teamRepository.findByTeamStatus(status);
    }

    public Team findByTeamName(String tn) {
        return teamRepository.findByTeamName(tn);
    }

    public Team inactivateTeam(Team t) {
        t.setStatusInactive();
        return teamRepository.save(t);
    }

    /* END OF TEAM METHODS */
    public WorkItem saveWorkItem(WorkItem workItem) {
        if (workItem.getIssue() != null && !workItem.getWorkItemStatus().equals(Status.Done)) {
            throw new ServiceException("Can not add issue to workItem that is not done");
        }

        if (workItem.getIssue() != null) {
            workItem.setStatusUnstarted();
        }

        return workItemRepository.save(workItem);
    }

    public void deleteWorkItem(WorkItem workItem) {
        workItemRepository.delete(workItem);
    }

    public Collection<WorkItem> searchForWorkItemByDescription(String desc) {
        return workItemRepository.findByDescriptionContaining(desc);
    }

    public Collection<WorkItem> findWorkItemsByStatus(String status) {
        return workItemRepository.findByWorkItemStatus(status);
    }

    public Collection<WorkItem> findWorkItemsByUser(User u) {
        return workItemRepository.findByUser(u);
    }

    public Collection<User> searchForUser(String search) {
        return userRepository.findByUsernameOrFirstNameOrLastNameContaining(search);
    }

    public WorkItem findWorkItemById(Long id) {
        return workItemRepository.findOne(id);
    }

    public Collection<WorkItem> findAllWorkItems() {
        return workItemRepository.findAll();
    }

    public Collection<WorkItem> findWorkItemsByTeam(Team t) {
        return workItemRepository.findByTeam(t);
    }

    public Collection<WorkItem> findAllWorkItemsWithIssue() {
        return workItemRepository.findByIssueNotNull();
    }

}
