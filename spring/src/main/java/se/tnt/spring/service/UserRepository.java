package se.tnt.spring.service;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;

import java.util.Collection;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u left join fetch u.workItems where u.userNumber = ?1")
    User findByUserNumber(String userNumber);

    Collection<User> findByFirstName(String firstName);

    Collection<User> findByLastName(String lastName);

    Collection<User> findByFirstNameAndLastName(String firstName, String lastName);

    @Query("select u from User u left join fetch u.workItems where u.username = ?1")
    User findByUsername(String username);

    @Query("select u from User u left join fetch u.workItems where u.username = ?1 or u.firstName = ?1 or u.lastName = ?1")
    Collection<User> findByUsernameOrFirstNameOrLastNameContaining(String search);

    Collection<User> findByTeam(Team t);

}
