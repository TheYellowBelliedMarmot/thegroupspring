package se.tnt.spring.model;

import se.tnt.spring.exception.ServiceException;
import se.tnt.spring.status.Status;

import javax.persistence.*;
import java.util.Collection;
import java.util.TreeSet;

@Entity
public class User extends AbstractEntity {

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @ManyToOne(cascade = CascadeType.ALL)
    private Team team;

    @Column
    private String userStatus;

    @Column
    private String userNumber;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<WorkItem> workItems;

    protected User() {
        workItems = new TreeSet<>();
    }

    public User(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userStatus = Status.Active;
        this.userNumber = Integer.toString(username.hashCode());
        workItems = new TreeSet();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Team getTeam() {
        return team;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public User deleteWorkItem(WorkItem workItem) {
        this.workItems.remove(workItem);
        return this;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setStatusInactive() {
        makeAllWorkItemsUnstarted();

        this.userStatus = Status.Inactive;
    }

    private void makeAllWorkItemsUnstarted() {
        workItems.forEach(workItem -> workItem.setStatusUnstarted());
    }

    public User addWorkItem(WorkItem workItem) {
        workItem.addUser(this);
        this.workItems.add(workItem);
        return this;
    }

    public Collection<WorkItem> getWorkItems() {
        return new TreeSet<>(workItems);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username != null ? username.equals(user.username) : user.username == null;

    }

    @Override
    public int hashCode() {
        return username != null ? username.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.getUsername();
    }

    public void addTeam(Team team) {
        this.team = team;
    }

}
