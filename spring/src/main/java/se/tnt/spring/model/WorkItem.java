package se.tnt.spring.model;


import se.tnt.spring.status.Status;

import javax.persistence.*;

@Entity
public class WorkItem extends AbstractEntity implements Comparable<WorkItem> {

    @Column(nullable = false)
    private String description;

    @Embedded
    private Issue issue;

    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    @Column
    private String workItemStatus;

    protected WorkItem() {
        this.workItemStatus = Status.Unstarted;
    }

    public WorkItem(String description) {
        this.description = description;
        this.workItemStatus = Status.Unstarted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkItemStatus() {
        return workItemStatus;
    }

    public void setStatusUnstarted() {
        this.workItemStatus = Status.Unstarted;
    }

    public void setStatusStarted() {
        this.workItemStatus = Status.Started;
    }

    public void setStatusDone() {
        this.workItemStatus = Status.Done;
    }

    public User getUser() {
        return user;
    }

    public Issue getIssue() {
        return issue;
    }

    public void addIssue(Issue issue) {
        this.issue = issue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkItem workItem = (WorkItem) o;

        return description != null ? description.equals(workItem.description) : workItem.description == null;
    }

    @Override
    public int hashCode() {
        return description != null ? description.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.getDescription();
    }

    @Override
    public int compareTo(WorkItem o) {
        return getDescription().compareTo(o.getDescription());
    }

    public void addUser(User user) {
        this.user = user;
    }

}
