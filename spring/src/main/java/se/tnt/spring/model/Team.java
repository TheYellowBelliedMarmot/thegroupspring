package se.tnt.spring.model;

import se.tnt.spring.status.Status;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.HashSet;

@Entity
public class Team extends AbstractEntity {

    @Column(nullable = false)
    private String teamName;

    @Column
    private String teamStatus;

    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
    private Collection<User> users;

    protected Team() {
    }

    public Team(String teamName) {
        this.teamName = teamName;
        this.teamStatus = Status.Active;
        users = new HashSet();
    }

    public String getTeamName() {
        return teamName;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setStatusInactive() {
        this.teamStatus = Status.Inactive;
    }

    public Team addUser(User u) {
        u.addTeam(this);
        this.users.add(u);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        return teamName != null ? teamName.equals(team.teamName) : team.teamName == null;
    }

    @Override
    public int hashCode() {
        return teamName != null ? teamName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.getTeamName();
    }

    public void clearUsers() {
        this.users.clear();
    }
}
