package se.tnt.spring.status;

public final class Status {

    private Status() {
    }

    public static final String Inactive = "-1";
    public static final String Active = "0";
    public static final String Unstarted = "1";
    public static final String Started = "2";
    public static final String Done = "3";

}
