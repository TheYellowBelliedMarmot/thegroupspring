package se.tnt.spring.validator;

import se.tnt.spring.model.Team;
import se.tnt.spring.model.User;
import se.tnt.spring.model.WorkItem;
import se.tnt.spring.status.Status;

public final class Validator {

    public static boolean valid(User u) {
        if (u.getWorkItems().size() > 0 && u.getUserStatus().equals(Status.Inactive)) {
            return false;
        }

        if (u.getWorkItems().size() >= 5) {
            return false;
        }

        return u.getUsername().trim().length() >= 10;
    }

    public static boolean valid(Team t) {
        return t.getUsers().size() <= 10;
    }

}
