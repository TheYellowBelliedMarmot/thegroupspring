package se.tnt.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import se.tnt.spring.model.User;
import se.tnt.spring.service.UserRepository;

public class Main {

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
            context.scan("se.tnt.spring");
            context.refresh();

            UserRepository userRepository = context.getBean(UserRepository.class);

            User user = userRepository.save(new User("fghjk", "fghjklö", "dfghjkl.ö", "xcgvbhjknl"));
            System.out.println(userRepository.findByUserNumber(user.getUserNumber()));
        }
    }
}
